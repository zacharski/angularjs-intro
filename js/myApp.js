var app = angular.module('myApp', []);

app.config(['$interpolateProvider', function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
}]);

app.controller('MyCtrl', function($scope){
    $scope.name = '';
    $scope.warning = 'no warnings';
    $scope.responses =[];
    $scope.responses2 =[];
    $scope.msg_count = 0;
    $scope.buttonText = "Hide";
    $scope.visible = true;

    $scope.processText = function() {
        console.log("In processText");
        $scope.warning = "WARNING: " + $scope.some_text;
        //$scope.$apply();
     }

     $scope.processMessage = function() {
        $scope.responses.push($scope.msg);
        //$scope.$apply();
     }
    
     $scope.processMessage2 = function() {
        $scope.responses2.push({'data': $scope.msg2, 'count': $scope.msg_count});
        $scope.msg_count += 1;

        //$scope.$apply();
     }
   
     $scope.showHide = function() {
        console.log("in ShowHide");
        if ($scope.buttonText == "Hide"){
            $scope.buttonText = "Show";
            $scope.visible = false;
        }
        else {
            $scope.visible = true;
            $scope.buttonText = "Hide"
        }
      }
     
});

console.log("HELLO")